<!DOCTYPE html>
<html lang="pt-br">

<head>
   <meta charset="UTF-8"/>
   <title>Home</title>
   

<script src="js/teste/froogaloop2.min.js?1b636-1354269188"></script>

   <link rel="stylesheet" href="css/jquery.mobile-1.4.5.min.css" />
   <script src="js/jquery-1.12.0.min.js"></script>
   <script src="js/jquery.mobile-1.4.5.min.js"></script>



<script>
$( document ).on( "pageinit", function() {
	$('#popupVideo').show();
    $( "#popupVideo iframe" )
        .attr( "width", 0 )
        .attr( "height", 0 );

    $( "#popupVideo" ).on({
        popupbeforeposition: function() {
            var size = scale( 497, 298, 15, 1 ),
                w = size.width,
                h = size.height;

            $( "#popupVideo iframe" )
                .attr( "width", w )
                .attr( "height", h );
        },
        popupafterclose: function() {
            $( "#popupVideo iframe" )
                .attr( "width", 0 )
                .attr( "height", 0 );
        }
    });
});

function scale( width, height, padding, border ) {
    var scrWidth = $( window ).width() - 30,
        scrHeight = $( window ).height() - 30,
        ifrPadding = 2 * padding,
        ifrBorder = 2 * border,
        ifrWidth = width + ifrPadding + ifrBorder,
        ifrHeight = height + ifrPadding + ifrBorder,
        h, w;

    if ( ifrWidth < scrWidth && ifrHeight < scrHeight ) {
        w = ifrWidth;
        h = ifrHeight;
    } else if ( ( ifrWidth / scrWidth ) > ( ifrHeight / scrHeight ) ) {
        w = scrWidth;
        h = ( scrWidth / ifrWidth ) * ifrHeight;
    } else {
        h = scrHeight;
        w = ( scrHeight / ifrHeight ) * ifrWidth;
    }

    return {
        'width': w - ( ifrPadding + ifrBorder ),
        'height': h - ( ifrPadding + ifrBorder )
    };
};
</script>

<script>
$(document).ready(function(){
	$('#popupVideo').hide();
})

</script>

    <script>
   var myVideo=document.getElementById("custom_video_play");
   function makeBig() {
	   myVideo.width=650;
    }
   function makeSmall(){
	   myVideo.width=350;
	}
   function makeNormal() {
	   myVideo.width=450;
	}
	</script>
   
</head>
<body>

            

	
	
	
	<div data-role="collapsible-set" data-theme="c" data-content-theme="d" data-inset="false">
    <div data-role="collapsible">
        <h2>Domingo</h2>
        <ul data-role="listview">
	        <li><a href="#popupVideo1" data-rel="popup" data-position-to="window" data-role="button"  data-inline="true">Nome do exercicio</a>
	
            	<div data-role="popup" id="popupVideo1" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" width="497" height="298">
                <source src="videos/teste2.mov" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>
				
			</li>

			
			
			
			
			
			
        </ul>
    </div>
	

	
	
	
	
	
</body>
</html>