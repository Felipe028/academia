<?php
$host = "localhost";
$user = "root";
$pass = "123";
$banco = "academia";
$conexao = mysql_connect($host, $user, $pass) or die(mysql_error());
mysql_select_db($banco) or die(mysql_error());
?>

<!--Verifica Login-->
<?php
session_start();
$idAluno = $_SESSION['idAluno'];
$Nome_aluno = $_SESSION['Nome_aluno'];

if(!isset($_SESSION["email_aluno"]) || !isset($_SESSION["senha_aluno"])){
	header("Location:Login.php");
	exit;
}
?>
<!--/Verifica Login-->


<head>
	<title>Arena Fitness</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
   <link rel="stylesheet" href="css/jquery.mobile-1.4.5.min.css" />
   <script src="js/jquery-1.12.0.min.js"></script>
   <script src="js/jquery.mobile-1.4.5.min.js"></script>
   <link rel="stylesheet" href="css/estilo.css" />
   
   <script>
$(document).on("pageinit", function () {
  $("#popupVideo iframe")
    .attr("width", 0)
    .attr("height", 0);

  $("#popupVideo").on({
    popupbeforeposition: function () {
      var size = scale(497, 298, 15, 1),
        w = size.width,
        h = size.height;

      $("#popupVideo iframe")
        .attr("width", w)
        .attr("height", h);
    },
    popupafterclose: function () {
      $("#popupVideo iframe")
        .attr("width", 0)
        .attr("height", 0);
      Froogaloop($('#player_1')[0]).api('pause');
    }
  });
});

function scale(width, height, padding, border) {
  var scrWidth = $(window).width() - 30,
    scrHeight = $(window).height() - 30,
    ifrPadding = 2 * padding,
    ifrBorder = 2 * border,
    ifrWidth = width + ifrPadding + ifrBorder,
    ifrHeight = height + ifrPadding + ifrBorder,
    h, w;

  if (ifrWidth < scrWidth && ifrHeight < scrHeight) {
    w = ifrWidth;
    h = ifrHeight;
  } else if ((ifrWidth / scrWidth) > (ifrHeight / scrHeight)) {
    w = scrWidth;
    h = (scrWidth / ifrWidth) * ifrHeight;
  } else {
    h = scrHeight;
    w = (scrHeight / ifrHeight) * ifrWidth;
  }

  return {
    'width': w - (ifrPadding + ifrBorder),
      'height': h - (ifrPadding + ifrBorder)
  };
};
</script>
	
</head>

<body>

<div data-role="header" id="bar1">
    <a href="Index.php?p=tabelas" id="voltar" data-icon="arrow-l">Voltar</a>
    <h1>Arena Fitness</h1>
    <a href="Logout.php" id="sair" data-icon="power">Sair</a>
</div>

<?php
$idTabela = $_GET['idTabela'];

if(empty($idTabela)){
	$selecionaDados = mysql_query("SELECT * FROM tabela ORDER BY idTabela DESC LIMIT 1");
    $campos = mysql_fetch_array($selecionaDados);
}else{
	$selecionaDados = mysql_query("SELECT * FROM tabela WHERE idTabela = '$idTabela'");
    $campos = mysql_fetch_array($selecionaDados);
	
	$idfunc = $campos['Funcionario_idFuncionario'];
	$selecionaDadosFunc = mysql_query("SELECT * FROM funcionario WHERE idFuncionario = '$idfunc'");
    $camposFunc = mysql_fetch_array($selecionaDadosFunc);
	
	$nomefunc = $camposFunc['Nome_func'];
}
?>
<br/>
<table style="border: 1px solid #4F0000; border-spacing: 0px; margin-left: auto; margin-right: auto;">

<caption style="border: 1px solid #4F0000; padding: 12px; color: #ffffff; background-color: #4F0000; text-shadow: 0 1px 0 #4F0000"><span style="font-weight:bold;">Tabela</span></caption>
   <tr>
   <td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #960000; text-shadow: 0 1px 0 #4F0000"><span style="font-weight:bold;">Instrutor</span></td>
   <td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #c90000; text-shadow: 0 1px 0 #c90000"><span style="font-weight:bold;"><?php echo $nomefunc;?></span></td>
   </tr>


   <tr><td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #960000; text-shadow: 0 1px 0 #4F0000"><span style="font-weight:bold;">Objetivo</span></td> <td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #c90000; text-shadow: 0 1px 0 #c90000"><span style="font-weight:bold;"><?php echo $campos['Objetivo'];?></span></td> </tr>


   <tr><td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #960000; text-shadow: 0 1px 0 #4F0000"><span style="font-weight:bold;">Intervalo</span></td> <td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #c90000; text-shadow: 0 1px 0 #c90000"><span style="font-weight:bold;"><?php echo $campos['Intervalo'];?></span></td> </tr>


   <tr><td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #960000; text-shadow: 0 1px 0 #4F0000"><span style="font-weight:bold;">Velocidade</span></td> <td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #c90000; text-shadow: 0 1px 0 #c90000"><span style="font-weight:bold;"><?php echo $campos['Velocidade'];?></span></td> </tr>


   <tr><td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #960000; text-shadow: 0 1px 0 #4F0000"><span style="font-weight:bold;">Início</span></td> <td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #c90000; text-shadow: 0 1px 0 #c90000"><span style="font-weight:bold;"><?php echo date("d-m-Y", strtotime($campos['Inicio']));?></span></td> </tr>


   <tr><td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #960000; text-shadow: 0 1px 0 #4F0000"><span style="font-weight:bold;">Reavaliação</span></td> <td style="border: 1px solid #4F0000; padding: 10px; color: #ffffff; background-color: #c90000; text-shadow: 0 1px 0 #c90000"><span style="font-weight:bold;"><?php echo date("d-m-Y", strtotime($campos['Reavaliacao']));?></span></td> </tr>
</table>

<?php
$selicionarExecDom = mysql_query("SELECT * FROM tabela_exercicios WHERE Tabela_idTabela = '$idTabela'");
$selicionarExecSeg = mysql_query("SELECT * FROM tabela_exercicios WHERE Tabela_idTabela = '$idTabela'");
$selicionarExecTer = mysql_query("SELECT * FROM tabela_exercicios WHERE Tabela_idTabela = '$idTabela'");
$selicionarExecQua = mysql_query("SELECT * FROM tabela_exercicios WHERE Tabela_idTabela = '$idTabela'");
$selicionarExecQui = mysql_query("SELECT * FROM tabela_exercicios WHERE Tabela_idTabela = '$idTabela'");
$selicionarExecSex = mysql_query("SELECT * FROM tabela_exercicios WHERE Tabela_idTabela = '$idTabela'");
$selicionarExecSab = mysql_query("SELECT * FROM tabela_exercicios WHERE Tabela_idTabela = '$idTabela'");
    ?>
	
<div data-role="collapsibleset" data-theme="d" data-content-theme="b">

    <div data-role="collapsible">
        <h2>Domingo</h2>
        <ul data-role="listview">
	<?php	
while($camposDom = mysql_fetch_array($selicionarExecDom)){
	if($camposDom['Dia_idDia'] == 1){
		$idExecDom = $camposDom['Exercicios_idExercicios'];
		$selicionarNomeExecDom = mysql_query("SELECT * FROM exercicios WHERE idExercicios = '$idExecDom'");
		$camposNomeExecDom = mysql_fetch_array($selicionarNomeExecDom);
	?>
	        <li><a href="#popupVideoDom<?php echo $camposNomeExecDom['idExercicios'];?>" id="diaex" data-rel="popup" data-position-to="window" data-inline="true">
                <h2><?php echo $camposNomeExecDom['Nome_exec'];?></h2>
                <p>Series: <?php echo $camposDom['Series'];?></p>
				<p>Repeticoes: <?php echo $camposDom['Repeticoes'];?></p>
				<p>Carga: <?php echo $camposDom['Carga'];?></p></a>
				
				
				
				<div data-role="popup" id="popupVideoDom<?php echo $camposNomeExecDom['idExercicios'];?>" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" style="width: 100%;">
                <source src="../academia2/videos/<?php echo $camposNomeExecDom['Nome_exec'];?>" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>
				
			</li>
	<?php } } ?>
        </ul>
    </div>

    <div data-role="collapsible">
        <h2>Segunda</h2>
        <ul data-role="listview">
	<?php	
while($camposSeg = mysql_fetch_array($selicionarExecSeg)){
	if($camposSeg['Dia_idDia'] == 2){
		$idExecSeg = $camposSeg['Exercicios_idExercicios'];
		$selicionarNomeExecSeg = mysql_query("SELECT * FROM exercicios WHERE idExercicios = '$idExecSeg'");
		$camposNomeExecSeg = mysql_fetch_array($selicionarNomeExecSeg);
	?>
	        <li><a href="#popupVideoSeg<?php echo $camposNomeExecSeg['idExercicios'];?>" id="diaex" data-rel="popup" data-position-to="window" data-inline="true">
                <h2><?php echo $camposNomeExecSeg['Nome_exec'];?></h2>
                <p>Series: <?php echo $camposSeg['Series'];?></p>
				<p>Repeticoes: <?php echo $camposSeg['Repeticoes'];?></p>
				<p>Carga: <?php echo $camposSeg['Carga'];?></p></a>
				
				
				
				<div data-role="popup" id="popupVideoSeg<?php echo $camposNomeExecSeg['idExercicios'];?>" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" style="width: 100%;">
                <source src="../academia2/videos/<?php echo $camposNomeExecSeg['Nome_exec'];?>" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>
				
			</li>
	<?php } } ?>
        </ul>
    </div>

    <div data-role="collapsible">
        <h2>Terca</h2>
        <ul data-role="listview">
	<?php	
while($camposTer = mysql_fetch_array($selicionarExecTer)){
	if($camposTer['Dia_idDia'] == 3){
		$idExecTer = $camposTer['Exercicios_idExercicios'];
		$selicionarNomeExecTer = mysql_query("SELECT * FROM exercicios WHERE idExercicios = '$idExecTer'");
		$camposNomeExecTer = mysql_fetch_array($selicionarNomeExecTer);
	?>
	        <li><a href="#popupVideoTer<?php echo $camposNomeExecTer['idExercicios'];?>" id="diaex" data-rel="popup" data-position-to="window" data-inline="true">
                <h2><?php echo $camposNomeExecTer['Nome_exec'];?></h2>
                <p>Series: <?php echo $camposTer['Series'];?></p>
				<p>Repeticoes: <?php echo $camposTer['Repeticoes'];?></p>
				<p>Carga: <?php echo $camposTer['Carga'];?></p></a>

				<div data-role="popup" id="popupVideoTer<?php echo $camposNomeExecTer['idExercicios'];?>" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" style="width: 100%;">
                <source src="../academia2/videos/<?php echo $camposNomeExecTer['Nome_exec'];?>" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>
				
			</li>
	<?php } } ?>
        </ul>
    </div>

    <div data-role="collapsible">
        <h2>Quarta</h2>
        <ul data-role="listview">
	<?php	
while($camposQua = mysql_fetch_array($selicionarExecQua)){
	if($camposQua['Dia_idDia'] == 4){
		$idExecQua = $camposQua['Exercicios_idExercicios'];
		$selicionarNomeExecQua = mysql_query("SELECT * FROM exercicios WHERE idExercicios = '$idExecQua'");
		$camposNomeExecQua = mysql_fetch_array($selicionarNomeExecQua);
	?>
	        <li><a href="#popupVideoQua<?php echo $camposNomeExecQua['idExercicios'];?>" id="diaex" data-rel="popup" data-position-to="window" data-inline="true">
                <h2><?php echo $camposNomeExecQua['Nome_exec'];?></h2>
                <p>Series: <?php echo $camposQua['Series'];?></p>
				<p>Repeticoes: <?php echo $camposQua['Repeticoes'];?></p>
				<p>Carga: <?php echo $camposQua['Carga'];?></p></a>

				<div data-role="popup" id="popupVideoQua<?php echo $camposNomeExecQua['idExercicios'];?>" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" style="width: 100%;">
                <source src="../academia2/videos/<?php echo $camposNomeExecQua['Nome_exec'];?>" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>				
				
			</li>
	<?php } } ?>
        </ul>
    </div>

    <div data-role="collapsible">
        <h2>Quinta</h2>
        <ul data-role="listview">
	<?php	
while($camposQui = mysql_fetch_array($selicionarExecQui)){
	if($camposQui['Dia_idDia'] == 5){
		$idExecQui = $camposQui['Exercicios_idExercicios'];
		$selicionarNomeExecQui = mysql_query("SELECT * FROM exercicios WHERE idExercicios = '$idExecQui'");
		$camposNomeExecQui = mysql_fetch_array($selicionarNomeExecQui);
	?>
	        <li><a href="#popupVideoQui<?php echo $camposNomeExecQui['idExercicios'];?>" id="diaex" data-rel="popup" data-position-to="window" data-inline="true">
                <h2><?php echo $camposNomeExecQui['Nome_exec'];?></h2>
                <p>Series: <?php echo $camposQui['Series'];?></p>
				<p>Repeticoes: <?php echo $camposQui['Repeticoes'];?></p>
				<p>Carga: <?php echo $camposQui['Carga'];?></p></a>
		 
				<div data-role="popup" id="popupVideoQui<?php echo $camposNomeExecQui['idExercicios'];?>" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" style="width: 100%;">
                <source src="../academia2/videos/<?php echo $camposNomeExecQui['Nome_exec'];?>" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>		 
		 
			</li>
	<?php } } ?>
        </ul>
    </div>

    <div data-role="collapsible">
        <h2>Sexta</h2>
        <ul data-role="listview">
	<?php	
while($camposSex = mysql_fetch_array($selicionarExecSex)){
	if($camposSex['Dia_idDia'] == 6){
		$idExecSex = $camposSex['Exercicios_idExercicios'];
		$selicionarNomeExecSex = mysql_query("SELECT * FROM exercicios WHERE idExercicios = '$idExecSex'");
		$camposNomeExecSex = mysql_fetch_array($selicionarNomeExecSex);
	?>
	        <li><a href="#popupVideoSex<?php echo $camposNomeExecSex['idExercicios'];?>" id="diaex" data-rel="popup" data-position-to="window" data-inline="true">
                <h2><?php echo $camposNomeExecSex['Nome_exec'];?></h2>
                <p>Series: <?php echo $camposSex['Series'];?></p>
				<p>Repeticoes: <?php echo $camposSex['Repeticoes'];?></p>
				<p>Carga: <?php echo $camposSex['Carga'];?></p></a>
		    
				<div data-role="popup" id="popupVideoSex<?php echo $camposNomeExecSex['idExercicios'];?>" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" style="width: 100%;">
                <source src="../academia2/videos/<?php echo $camposNomeExecSex['Nome_exec'];?>" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>			
			
			</li>
	<?php } } ?>
        </ul>
    </div>

    <div data-role="collapsible">
        <h2>Sabado</h2>
        <ul data-role="listview">
	<?php	
while($camposSab = mysql_fetch_array($selicionarExecSab)){
	if($camposSab['Dia_idDia'] == 7){
		$idExecSab = $camposSab['Exercicios_idExercicios'];
		$selicionarNomeExecSab = mysql_query("SELECT * FROM exercicios WHERE idExercicios = '$idExecSab'");
		$camposNomeExecSab = mysql_fetch_array($selicionarNomeExecSab);
	?>
	        <li><a href="#popupVideoSab<?php echo $camposNomeExecSab['idExercicios'];?>" id="diaex" data-rel="popup" data-position-to="window" data-inline="true">
                <h2><?php echo $camposNomeExecSab['Nome_exec'];?></h2>
                <p>Series: <?php echo $camposSab['Series'];?></p>
				<p>Repeticoes: <?php echo $camposSab['Repeticoes'];?></p>
				<p>Carga: <?php echo $camposSab['Carga'];?></p></a>
		    
				<div data-role="popup" id="popupVideoSab<?php echo $camposNomeExecSab['idExercicios'];?>" data-overlay-theme="a" data-theme="d" data-tolerance="15,15" class="ui-content">
				<video controls="true" style="width: 100%;">
                <source src="../academia2/videos/<?php echo $camposNomeExecSab['Nome_exec'];?>" type="video/mp4" width="497" height="298"/>
                </video>				
                </div>			
			
			</li>
	<?php } } ?>
        </ul>
    </div>

</div>

</body>
</html>