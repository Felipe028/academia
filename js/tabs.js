﻿/***************************/
//@Author: Adrian "yEnS" Mato Gondelle & Ivan Guardado Castro
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

$(document).ready(function(){
	
	
	$("#menu-home").click(function(){
	
		$("#menu-home").addClass("ativado");
				$("#menu-fun").removeClass("ativado");
				$("#menu-alu").removeClass("ativado");
				$("#menu-exer").removeClass("ativado");
				
	});
	
	$("#menu-fun").click(function(){
	
		$("#menu-fun").addClass("ativado");
				$("#menu-home").removeClass("ativado");
				$("#menu-alu").removeClass("ativado");
				$("#menu-exer").removeClass("ativado");
				
	});
	
	
	
	$("#menu-alu").click(function(){
	
		$("#menu-alu").addClass("ativado");
				$("#menu-home").removeClass("ativado");
				$("#menu-fun").removeClass("ativado");
				$("#menu-exer").removeClass("ativado");
				
	});
	
	$("#menu-exer").click(function(){
	
		$("#menu-exer").addClass("ativado");
				$("#menu-home").removeClass("ativado");
				$("#menu-fun").removeClass("ativado");
				$("#menu-alu").removeClass("ativado");
				
	});
	
	
});