<?php
$host = "localhost";
$user = "root";
$pass = "123";
$banco = "academia";
$conexao = mysql_connect($host, $user, $pass) or die(mysql_error());
mysql_select_db($banco) or die(mysql_error());
?>
<!--Verifica Login-->
<?php
session_start();
$idFuncionario = $_SESSION['idFuncionario'];
$Nome_func = $_SESSION['Nome_func'];
$tipo = $_SESSION['tipo'];
$email = $_SESSION["email_func"];

if(!isset($_SESSION["email_func"]) || !isset($_SESSION["senha_func"])){
	header("Location:Login.php");
	exit;
}

if (isset($_SESSION["sessiontime"] ) ) { 
	if ($_SESSION["sessiontime"] < time() ) { 
		echo "Seu tempo Expirou!";
		header("Location:Login.php");
	    exit;
	} else {
		//Seta mais tempo 60 segundos
		$_SESSION["sessiontime"] = time() + 900;
	}
}
?>
<!--/Verifica Login-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="img/logo.png">
<link href="css/estilo.css" rel="stylesheet" type="text/css">

<!--reveal-->
<link rel="stylesheet" href="css/reveal.css">	
	  	
<!-- Attach necessary scripts -->
<!-- <script type="text/javascript" src="jquery-1.4.4.min.js"></script> -->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>


<title>Fitness</title>
</head>

<body>
<div class="geral">
	
    <div class="topo">
    	 <center><img width="400" src="img/Arena.png"/></center>
        
    
        
        <ul>
			<li><a  href="Index.php?p=home"><img  src="img/home.png"/></a></li>
			<li><a  href="Index.php?p=fun"><img  src="img/fun.png"/></a></li>
			<li><a href="Index.php?p=alu"><img  src="img/alu.png"/> </a></li>
			<li><a href="Index.php?p=exer"><img  src="img/exer.png"/></a></li>
			<li ><a style="margin:0 0 0 132px;" href="Logout.php" title="Sair" ><img  src="img/sair.png"/></a></li>
		</ul>
        
    </div><br/>
    
    <div class="conteudo">
    	
        <?php
		include "conexao.php";
		
		include "pagina.php";
		
		?>
	
    
    </div>


</div>

</body>
</html>